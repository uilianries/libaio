# -*- coding: utf-8 -*
from conans import ConanFile, tools, CMake


class civetwebConan(ConanFile):
    name = "libaio"
    license = "LGLP-2.1"
    exports = ("COPYING")
    exports_sources = ("src/*", "CMakeLists.txt")
    generators = "cmake"
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared"            : [True, False],
        "fPIC"              : [True, False]
    }
    default_options = {
        "shared"            : False,
        "fPIC"              : True
    }

    def config_options(self):
        if self.settings.os == 'Windows':
            del self.options.fPIC

    def configure(self):
        del self.settings.compiler.libcxx

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.configure(build_dir="build_subfolder")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy("COPYING", dst="licenses")
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
